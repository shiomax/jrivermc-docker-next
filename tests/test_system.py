#! /usr/bin/python3

from os import environ
from testcontainers.core.container import DockerContainer

image_name=environ.get('TEST_IMAGE', default = None)

def test_apt_working():
    """
    Tests if apt is working out of the box. Updating the system or installing new packages.
    """
    with DockerContainer(image_name).with_env("SECURE_CONNECTION", "0") as container:
        exit_code_update = container.exec("apt-get update").exit_code
        exit_code_upgrade = container.exec("apt-get upgrade -y").exit_code
        container.exec("apt-get install -y vim")
        stdout_which = str(container.exec("which vim").output)

        assert exit_code_update == 0, "apt-update exited with non-zero status code"
        assert exit_code_upgrade == 0, "apt-upgrade existed with non-zero status code"
        assert "/usr/bin/vim" in stdout_which, "vim was not installed"
