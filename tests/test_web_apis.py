#! /usr/bin/python3

from os import environ
import requests
from testcontainers.core.container import DockerContainer
from testutils import wait_for_http_get_ok, wait_for_container_ready

image_name=environ.get('TEST_IMAGE', default = None)

def test_init_endpoint():
    """
    Tests Init Endpoint of API. Should return JSON like
    {
        "canAdopt": true,
        "authRequired": true,
        "initialWidth": 1280,
        "initialHeight": 768
    }
    """
    with DockerContainer(image_name).with_env("SECURE_CONNECTION", "0").with_exposed_ports(5800) as container:
        wait_for_container_ready(container)
        
        url = f"http://{container.get_container_host_ip()}:{container.get_exposed_port(5800)}/api/general/init"
        wait_for_http_get_ok(url)

        response = requests.get(url)
        assert response.status_code == 200
        
        body = response.json()

        assert body.get('canAdopt', None) == True
        assert body.get('authRequired', None) == True
        assert body.get('initialWidth', None) == 1280
        assert body.get('initialHeight', None) == 768

def test_user_creation():
    """
    Tests creating initial User
    """
    with DockerContainer(image_name).with_env("SECURE_CONNECTION", "0").with_exposed_ports(5800) as container:
        wait_for_container_ready(container)

        # Create User
        adopt_url = f"http://{container.get_container_host_ip()}:{container.get_exposed_port(5800)}/api/auth/adopt"
        adopt_response = requests.post(adopt_url, json={
            "username": "NewUser",
            "password": "SuperSecurePassword123"
        })

        assert adopt_response.status_code == 200
        adopt_body = adopt_response.json()
        assert adopt_body.get('success', None) == True
        assert adopt_body.get('errors', None) == []

        # Login
        login_url = f"http://{container.get_container_host_ip()}:{container.get_exposed_port(5800)}/api/auth/login"

        login_response = requests.post(login_url, json = {
            "username": "NewUser",
            "password": "SuperSecurePassword123"
        })

        assert login_response.status_code == 200
        login_body = login_response.json()
        assert login_body.get('token', None) != None
        assert len(login_body.get('token', "")) > 1
        assert login_body.get('refreshToken', None) != None
        assert len(login_body.get('refreshToken')) > 1

def test_disable_auth():
    """
    Check if DISABLE_AUTH option works
    """
    with DockerContainer(image_name).with_env("SECURE_CONNECTION", "0").with_exposed_ports(5800).with_env("DISABLE_AUTH", "1") as container:
        wait_for_container_ready(container)

        # Wait until Api Started
        init_url = f"http://{container.get_container_host_ip()}:{container.get_exposed_port(5800)}/api/general/init"
        wait_for_http_get_ok(init_url)

        # Check init response
        init_response = requests.get(init_url)
        assert init_response.status_code == 200

        init_body = init_response.json()
        assert init_body.get('canAdopt', None) == False
        assert init_body.get('authRequired', None) == False
        assert init_body.get('initialWidth', None) == 1280
        assert init_body.get('initialHeight', None) == 768

        # Create user (should fail now)
        adopt_url = f"http://{container.get_container_host_ip()}:{container.get_exposed_port(5800)}/api/auth/adopt"
        adopt_response = requests.post(adopt_url, json={
            "username": "NewUser",
            "password": "SuperSecurePassword123"
        })

        assert adopt_response.status_code == 200
        adopt_body = adopt_response.json()
        assert adopt_body.get('success', None) == False
        assert adopt_body.get('errors', None) == [ "Authentication is disabled." ]

def test_mcws_available():
    """
    Tests if MCs MCWS service is reachable.
    """
    with DockerContainer(image_name).with_env("SECURE_CONNECTION", "0").with_exposed_ports(52199) as container:
        wait_for_container_ready(container)
        
        url = f"http://{container.get_container_host_ip()}:{container.get_exposed_port(52199)}/MCWS/v1/Alive"
        wait_for_http_get_ok(url)

        response = requests.get(url)
        assert response.status_code == 200

        body = str(response.content)
        assert 'Status="OK"' in body

