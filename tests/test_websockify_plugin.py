#! /usr/bin/python3

from unittest import mock
import jwt
from jrmc_docker_token_plugin import JRMCDockerTokenPlugin
from datetime import datetime, timedelta
import calendar
import uuid
import os

def create_token_plugin(secret = "<test secret>"):
    return JRMCDockerTokenPlugin(secret)

def create_token(payload, secret = "<test secret>"):
    # Convert datetime objects to unix timestamps
    for key in payload:
        if isinstance(payload[key], datetime):
            payload[key] = calendar.timegm(payload[key].timetuple())
    return jwt.encode(payload, secret, algorithm="HS256")

def test_token_should_be_accepted():
    plugin = create_token_plugin()
    nowtime = datetime.utcnow()
    exp = nowtime + timedelta(hours=1)
    token = create_token({
        "jti": str(uuid.uuid4()),
        "id": 1,
        "nbf": nowtime, 
        "exp": exp,
        "iat": nowtime 
    })
    result = plugin.lookup(token)
    
    assert result == ("unix_socket", "/tmp/tigervnc.sock")

@mock.patch.dict(os.environ, { "VNC_LISTEN": "LOCALHOST" })
def test_ip_and_port_vnclisten_localhost():
    plugin = create_token_plugin()
    nowtime = datetime.utcnow()
    exp = nowtime + timedelta(hours=1)
    token = create_token({
        "jti": str(uuid.uuid4()),
        "id": 1,
        "nbf": nowtime, 
        "exp": exp,
        "iat": nowtime 
    })
    result = plugin.lookup(token)
    assert result == ("127.0.0.1", 5900)

def test_token_with_invalid_secret_should_be_rejected():
    plugin = create_token_plugin()
    nowtime = datetime.utcnow()
    exp = nowtime + timedelta(hours=1)
    token = create_token({
        "jti": str(uuid.uuid4()),
        "id": 1,
        "nbf": nowtime, 
        "exp": exp,
        "iat": nowtime 
    }, "Invalid Secret")
    result = plugin.lookup(token)
    assert result is None


def test_token_without_nbf_should_be_rejected():
    plugin = create_token_plugin()
    nowtime = datetime.utcnow()
    exp = nowtime + timedelta(hours=1)
    token = create_token({
        "jti": str(uuid.uuid4()),
        "id": 1,
        "exp": exp,
        "iat": nowtime 
    })
    result = plugin.lookup(token)
    assert result is None


def test_token_without_exp_should_be_rejected():
    plugin = create_token_plugin()
    nowtime = datetime.utcnow()
    token = create_token({
        "jti": str(uuid.uuid4()),
        "id": 1,
        "nbf": nowtime,
        "iat": nowtime 
    })
    result = plugin.lookup(token)
    assert result is None


def test_token_without_id_should_be_rejected():
    plugin = create_token_plugin()
    nowtime = datetime.utcnow()
    exp = nowtime + timedelta(hours=1)
    token = create_token({
        "jti": str(uuid.uuid4()),
        "nbf": nowtime, 
        "exp": exp,
        "iat": nowtime 
    })
    result = plugin.lookup(token)
    assert result is None


def test_token_with_nbf_in_future_should_be_rejected():
    plugin = create_token_plugin()
    nowtime = datetime.utcnow()
    exp = nowtime + timedelta(hours=1)
    nfb = nowtime + timedelta(hours=1)
    token = create_token({
        "jti": str(uuid.uuid4()),
        "id": 1,
        "exp": exp,
        "nbf": nfb,
        "iat": nowtime 
    })
    result = plugin.lookup(token)
    assert result is None


def test_token_with_exp_in_past_should_be_rejected():
    plugin = create_token_plugin()
    nowtime = datetime.utcnow()
    exp = nowtime + timedelta(hours = -1)
    token = create_token({
        "jti": str(uuid.uuid4()),
        "id": 1,
        "exp": exp,
        "nbf": nowtime,
        "iat": nowtime 
    })
    result = plugin.lookup(token)
    assert result is None
