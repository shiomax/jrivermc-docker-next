#!/bin/bash

# Checks dependencies of buildscript printing more helpful error messages

if ! command -v docker &> /dev/null; then
    echo "Docker is required, but not installed..."
    exit 1
fi

if ! docker info > /dev/null 2>&1; then
  echo "Cannot connect to docker deamon, make sure docker is running..."
  exit 1
fi

if ! command -v jq &> /dev/null; then
    echo "jq is required, but not installed..."
    exit 1
fi
