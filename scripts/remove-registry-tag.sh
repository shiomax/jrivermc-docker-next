#!/bin/bash

# Script that removes tags from a private docker registry.
# Because no delete tag api exists, finds all the layers of the tag, then filters the once safe to delete (not used by another image).
# Then pushes an empty image to the same tag and removes all layers of the empty image to effectively delete the tag.
# NOTE: This only cares about tagged images. It might still break untagged images refered to by digest.

usage() {
    if [ "$*" ]; then
        echo "$*"
        echo
    fi

    echo "usage: $( basename $0 ) [OPTIONS]

Options:
  -h, --help          Display this help.
  -i, --image-name    Name of the image (required)
  -t, --tag           Tag to be deleted (required)
  -r, --registry      IP or name of private docker registry to use.
  --use-https         Make requests with https instead of http.
"
}

PROTOCOL="http"

# Parse arguments.
while [[ $# > 0 ]]
do
    key="$1"

    case $key in
        -h|--help)
            usage
            exit 0
            ;;
        -i|--image-name)
            shift
            IMAGE_NAME="$1"
            ;;
        -t|--tag)
            shift
            IMAGE_TAG="$1"
            ;;
        -r|--registry)
            shift
            REGISTRY="$1"
            ;;
        --use-https)
            PROTOCOL="https"
            ;;
        -*)
            usage "ERROR: Unknown argument: $key"
            exit 1
            ;;
        *)
            break
            ;;
    esac
    shift
done

! [ "${IMAGE_NAME:-unset}" = "unset" ] || { echo "--image-name required, but not set."; exit 1; }
! [ "${IMAGE_TAG:-unset}" = "unset" ] || { echo "--tag required, but not set."; exit 1; }
! [ "${REGISTRY:-unset}" = "unset" ] || { echo "--registry required, but not set."; exit 1; }

# List all image repositories
get_repos() {
    curl --silent "$PROTOCOL://$REGISTRY/v2/_catalog" | jq --raw-output -M '.repositories | flatten[]'
}

# Lists all tags for image repository $1 except tag $IMAGE_TAG for repo $IMAGE_NAME
get_tags() {
    ALL_TAGS="$(curl --silent "$PROTOCOL://$REGISTRY/v2/$1/tags/list" | jq --raw-output -M '.tags | flatten[]')"

    if [ "$1" = "$IMAGE_NAME" ]; then
        echo "$ALL_TAGS"  | grep --ignore-case --invert-match "$IMAGE_TAG"
    else
        echo "$ALL_TAGS"
    fi
}

# Lists all unique layers for image repository $1 and tag $2
get_layers_for_tag() {
    curl --silent "$PROTOCOL://$REGISTRY/v2/$1/manifests/$2" | jq --raw-output -M '.fsLayers | map(.blobSum) | flatten[]' | sort | uniq -u
}

# Remove Layer $1
remove_layer() {
    curl -X DELETE --silent "$PROTOCOL://$REGISTRY/v2/$IMAGE_NAME/blobs/$1"
}

# Remove manifest $1
remove_manifest() {
    curl -X DELETE "$PROTOCOL://$REGISTRY/v2/$IMAGE_NAME/manifests/$1"
}

# Check if docker tag exists
docker_tag_exists() {
    curl --silent "$PROTOCOL://$REGISTRY/v2/$IMAGE_NAME/tags/list" | jq --raw-output -M '.tags | flatten[]' | grep -q --count --ignore-case $1
}

# Exit immidiately if docker tag does not exist
if ! docker_tag_exists $IMAGE_TAG; then
    echo "Docker tag $IMAGE_TAG does not exist"
    exit 0
fi

LAYERS_TO_DELETE="$(get_layers_for_tag "$IMAGE_NAME" "$IMAGE_TAG")"

# Filter layers used by another image
while read repo; do
    while read tag; do
        while read layer; do
            LAYERS_TO_DELETE="$(echo "$LAYERS_TO_DELETE" | grep --invert-match --ignore-case $layer)"
        done < <(get_layers_for_tag "$repo" "$tag")
    done < <(get_tags "$repo")
done < <(get_repos)

# Mark layers for deletion
while read layer; do
    echo "Marking layer $layer for deletion."
    remove_layer $layer
done < <(echo "$LAYERS_TO_DELETE")

EMPTY_IMAGE_TAG="$REGISTRY/$IMAGE_NAME:$IMAGE_TAG"

# Build Empty docker image
docker build -t "$EMPTY_IMAGE_TAG" \
    --build-arg tag="$EMPTY_IMAGE_TAG" \
    -f - . <<EOF
FROM scratch
ARG tag
ENV tag=\$tag
EOF

# Push image on top of current tag
PUSH_OUTPUT="$(docker push "$EMPTY_IMAGE_TAG")"
echo "$PUSH_OUTPUT"

PUSHED_DIGEST="$(echo "$PUSH_OUTPUT" | grep --only-matching "sha256:[^\s-]*" | tail -n 1 | tr -d '[:space:]')"

echo "Removing manifest with digest: $PUSHED_DIGEST"
remove_manifest "$PUSHED_DIGEST"

echo "Remove empty image locally..."
docker image rm --force "$EMPTY_IMAGE_TAG"
