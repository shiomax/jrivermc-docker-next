#!/bin/bash

set -e # Exit immediately if a command exits with a non-zero status.
set -u # Treat unset variables as an error.


cleanup_images() {
    TEMPORARY_IMAGE="$(./scripts/readvar.sh all TEMP_IMAGE_NAME)"
    DOCKERHUB_REPOSITORY="$(./scripts/readvar.sh all RELEASE_REPO)"

    ./scripts/cleanup.sh "$TEMPORARY_IMAGE" "$DOCKERHUB_REPOSITORY"
}

cleanup_images

echo "Loading images..."
./scripts/load-images.sh

AMD64_IMAGE="$(./scripts/readvar.sh amd64 TEMP_IMAGE_FULL_NAME)"
ARM64_IMAGE="$(./scripts/readvar.sh arm64 TEMP_IMAGE_FULL_NAME)"
DEB_URL_AMD64="$(./scripts/readvar.sh amd64 DEB_URL)"
DEB_URL_ARM64="$(./scripts/readvar.sh arm64 DEB_URL)"
STABLE_AVAILABLE="$(./scripts/readvar.sh all STABLE_AVAILABLE)"
IS_LATEST="$(./scripts/readvar.sh all IS_LATEST)"
IS_LATEST_STABLE="$(./scripts/readvar.sh all IS_LATEST_STABLE)"
REPOSITORY="$(./scripts/readvar.sh all RELEASE_REPO)"
JRIVER_RELEASE="$(./scripts/readvar.sh all JRIVER_RELEASE)"
IMAGE_VERSION="$(./scripts/readvar.sh all IMAGE_VERSION)"

echo "---------------------"
echo "VARIABLES"
echo "---------------------"
echo "AMD64_IMAGE:      $AMD64_IMAGE"
echo "ARM64_IMAGE:      $ARM64_IMAGE"
echo "DEB_URL_AMD64:    $DEB_URL_AMD64"
echo "DEB_URL_ARM64:    $DEB_URL_ARM64"
echo "JRIVER_TAG:       $JRIVER_TAG"
echo "STABLE_AVAILABLE: $STABLE_AVAILABLE"
echo "IS_LATEST:        $IS_LATEST"
echo "IS_LATEST_STABLE: $IS_LATEST_STABLE"
echo "REPOSITORY:       $REPOSITORY"
echo "JRIVER_RELEASE:   $JRIVER_RELEASE"
echo "IMAGE_VERSION:    $IMAGE_VERSION"
echo "--------------------"

push_image() {
    echo "Publishing image '$1'"

    # push amd64 image
    docker tag $AMD64_IMAGE "$1-amd64"
    docker push "$1-amd64"

    # push arm64 image
    docker tag $ARM64_IMAGE "$1-arm64"
    docker push "$1-arm64"

    # removing manifest just in case it exists
    docker manifest rm "$1" || true

    # push multiarch image
    docker manifest create "$1" \
        --amend "$1-amd64" \
        --amend "$1-arm64"

    docker manifest push --purge "$1"
}

# Push images using apt repository (latest, latest-v0.0.1, latest-v0.0.1)
if [ "$DEB_URL_AMD64" = "repository" ] && [ "$DEB_URL_ARM64" = "repository" ]; then
    echo "Publishing images using MC apt repository."

    # push fullname image like latest-29-v0.0.1
    push_image "$REPOSITORY:$JRIVER_TAG-$JRIVER_RELEASE-$IMAGE_VERSION"

    # push latest major version like latest:29
    push_image "$REPOSITORY:$JRIVER_TAG-$JRIVER_RELEASE"

    # push :latest
    if [ "$JRIVER_TAG" = "latest" ]; then
        if [ "$IS_LATEST" = "1" ]; then
            echo "Pushing image as :latest"
            push_image "$REPOSITORY:$JRIVER_TAG"
        else
            echo "Skipping :latest push, not the latest 'latest' mc version."
        fi
    fi

    # push :stable
    if [ "$JRIVER_TAG" = "stable" ]; then
        if [ "$IS_LATEST_STABLE" = "1" ]; then
            echo "Pushing image as :stable"
            push_image "$REPOSITORY:$JRIVER_TAG"
        else
            echo "Skipping :stable push, not the latest 'stable' mc version."
        fi
    fi

elif [ "$DEB_URL_AMD64" = "repository" ] || [ "$DEB_URL_ARM64" = "repository" ]; then
    echo "Only one of the images was built from repo?!"
    exit 1
else
    ARM64_VERSION="$(echo $DEB_URL_ARM64 | grep --only-matching --regexp "$JRIVER_RELEASE\.[0-9]\+\.[0-9]\+" || true)"
    AMD64_VERSION="$(echo $DEB_URL_AMD64 | grep --only-matching --regexp "$JRIVER_RELEASE\.[0-9]\+\.[0-9]\+" || true)"

    # Validate
    [ ! -z "$ARM64_VERSION" ] || { echo "Could not get version from arm64 deb url."; exit 1; }
    [ ! -z "$AMD64_VERSION" ] || { echo "Could not get version from amd64 deb url."; exit 1; }
    [ "$ARM64_VERSION" = "$AMD64_VERSION" ] || { echo "Versions do not match, found '$ARM64_VERSION' and '$AMD64_VERSION'"; exit 1; }

    # Push image using deb url like latest-29.0.66-v0.0.1
    push_image "$REPOSITORY:$JRIVER_TAG-$AMD64_VERSION-$IMAGE_VERSION"
fi

cleanup_images
./scripts/cleanup-registry.sh
